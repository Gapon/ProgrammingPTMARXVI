package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "AdminServlet", urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем фильмы
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        request.setAttribute("movieDTOList", movieDTOList);
        //получаем залы
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        request.setAttribute("hallDTOList", hallDTOList);
        //получаем сеансы
        List<SessionDTO> sessionDTOList = SessionServiceImpl.getInstance().getAll();
        request.setAttribute("sessionDTOList", sessionDTOList);
        request.getRequestDispatcher("pages/admin/admin.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
