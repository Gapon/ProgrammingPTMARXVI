package controllers;


import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "MovieFilterServlet", urlPatterns = {"/moviefilter"})
public class MovieFilterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем из запроса название и даты фильмов
        String title = request.getParameter("title");
        List<MovieDTO> movieListByStartDate = MovieServiceImpl.getInstance().getMovieByStartDate(request.getParameter("start"));
        List<MovieDTO> movieListByEndDate = MovieServiceImpl.getInstance().getMovieByEndDate(request.getParameter("end"));
        List<MovieDTO> movieByTitle = MovieServiceImpl.getInstance().getMovieByTitle(title);
        request.setAttribute("movieListByStartDate", movieListByStartDate);
        request.setAttribute("movieListByEndDate", movieListByEndDate);
        request.setAttribute("movieByTitle", movieByTitle);
        request.getRequestDispatcher("pages/common/moviefilter.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
