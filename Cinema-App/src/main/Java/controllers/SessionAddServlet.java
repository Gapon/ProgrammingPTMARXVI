package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "SessionAddServlet", urlPatterns={"/session_add"})
public class SessionAddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //вытягиваем из базы фильмы и залы
        List<MovieDTO> movieList = MovieServiceImpl.getInstance().getAll();
        List<HallDTO> hallList = HallServiceImpl.getInstance().getAll();
        request.setAttribute("movieList", movieList);
        request.setAttribute("hallList", hallList);
        request.getRequestDispatcher("pages/admin/session_add.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
