package dao.impl;

import datasource.DataSource;
import model.Role;
import model.User;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public final class UserDaoImpl extends CrudDAO<User> {
    public static final String INSERT_USER = "INSERT INTO user (login, password, first_name, last_name, email, sex, birthday, role_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_USER = "UPDATE user SET first_name = ?, last_name = ?, email = ?, sex = ?, birthday = ? WHERE id = ?";
    public static final String UPDATE_USER_NAME = "UPDATE user SET first_name = ? WHERE id = ?";
    public static final String GET_USER_BY_NAME = "SELECT * FROM user WHERE first_name = ?";
    public static final String GET_USER_BY_SEX = "SELECT * FROM user WHERE sex = ?";
    public static final String GET_USER_BY_EMAIL = "SELECT * FROM user WHERE email = ?";
    public static final String GET_USER_BY_LOGIN = "SELECT * FROM user WHERE login = ?";

    private static UserDaoImpl crudDAO;

    public UserDaoImpl(Class type) {
        super(type);
    }


    public static synchronized UserDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new UserDaoImpl(User.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString (2, entity.getSurname());
        preparedStatement.setString(3, entity.getEmail());
        preparedStatement.setString(4, entity.getSex());
        preparedStatement.setDate (5, Date.valueOf(entity.getBirthday()));
        preparedStatement.setInt(6, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getLogin());
        try {
            preparedStatement.setString(2, this.encryptPassword(entity.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        preparedStatement.setString(3, entity.getName());
        preparedStatement.setString (4, entity.getSurname());
        preparedStatement.setString(5, entity.getEmail());
        preparedStatement.setString(6, entity.getSex());
        preparedStatement.setDate (7, Date.valueOf(entity.getBirthday()));
        preparedStatement.setInt (8, 1);
        return preparedStatement;
    }

    @Override
    public List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        User user = null;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("first_name"));
            user.setSurname(resultSet.getString("last_name"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setSex(resultSet.getString("sex"));
            user.setBirthday(resultSet.getDate("birthday").toLocalDate());
            user.setEmail(resultSet.getString("email"));
            user.setRole(new RoleDaoImpl(Role.class).getById(resultSet.getInt("role_id")));
            result.add(user);
        }
        return result;
    }

    public void updateUserNameById(String name, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_NAME);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getUserByName(String name) {
        Connection connection = DataSource.getInstance().getConnection();
        List<User> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_NAME);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  result.get(0);
    }

    public List<User> getUserBySex (String sex) {
        Connection connection = DataSource.getInstance().getConnection();
        List<User> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_SEX);
            preparedStatement.setString(1, sex);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  result;
    }
    public User getUserByEmail (String email) {
        Connection connection = DataSource.getInstance().getConnection();
        List<User> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_EMAIL);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  result.get(0);
    }

    public User getUserByLogin (String login) {
        Connection connection = DataSource.getInstance().getConnection();
        List<User> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            return  result.get(0);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        final StringBuilder sbMd5Hash = new StringBuilder();
        final MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(password.getBytes("UTF-8"));

        final byte data[] = m.digest();

        for (byte element : data) {
            sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
            sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
        }

        return sbMd5Hash.toString();
    }

}
