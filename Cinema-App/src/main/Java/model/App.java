package model;//package com.cinema.model;
//
//import java.sql.*;
//import java.time.LocalDate;
//
//public class App {
//    public static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS movie(id int AUTO_INCREMENT primary key, "
//            + "title varchar(256), description varchar(256), duration int, rating INT, genre VARCHAR (256), start DATE)";
//    public static final String SQL_INSERT = "INSERT INTO movie(title, description, duration, rating, genre ,start) VALUES (?, ?, ?, ?, ?, ?)";
//    public static void main(String[] args) {
//        String url = "jdbc:mysql://localhost:3306/cinema";
////        String user = "root";
//        String password = "135369";
//        MovieDTO movie = new MovieDTO("Titanic", "Bla bla", 150, 9.0, "drama", LocalDate.now());
//        System.out.println(movie);
//
//        try (Connection connection = DriverManager.getConnection(url,user,password);
//             Statement statement = connection.createStatement()){
//            System.out.println(statement.execute(SQL_CREATE));
//            addMovie(connection, movie);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        System.out.println(movie);
//    }
//    public static void addMovie(Connection connection, MovieDTO movie){
//        try(PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)){
//            statement.setString(1, movie.getTitle());
//            statement.setString(2, movie.getDescription());
//            statement.setInt(3, movie.getDuration());
//            statement.setDouble(4, movie.getRating());
//            statement.setString(5, movie.getGenre());
//            statement.setDate(6, Date.valueOf(movie.getStart()));
//            statement.executeUpdate();
//            ResultSet rs = statement.getGeneratedKeys();
//            if(rs.next()){
//                movie.setId(rs.getInt(1));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//}