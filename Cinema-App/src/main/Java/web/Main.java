package web;

import dao.impl.*;
import model.Hall;
import model.Movie;
import model.Row;
import model.Session;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;


public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {


        CrudDAO<Movie> movieDao = new MovieDaoImpl(Movie.class);
        MovieDaoImpl movieDaoImpl = new MovieDaoImpl(Movie.class);

        CrudDAO<Hall> hallDao = new HallDaoImpl(Hall.class);
        HallDaoImpl hallDaoImpl = new HallDaoImpl(Hall.class);
        CrudDAO<Row> rowDAO = new RowDaoimpl(Row.class);
        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl(Session.class);


        Movie movie = new Movie("wwew", "ee", 120, LocalDate.of(2016, 6, 6), LocalDate.of(2016, 6, 7), "апвапв", 10.0);
        Movie movie1 = new Movie("Heavy New Year", "dfgfg", 120, LocalDate.of(2016, 6, 6), LocalDate.of(2016, 6, 7), "horror", 10.0);
        movieDao.upload(movie);

    }

    public static String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        final StringBuilder sbMd5Hash = new StringBuilder();
        final MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(password.getBytes("UTF-8"));

        final byte data[] = m.digest();

        for (byte element : data) {
            sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
            sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
        }

        return sbMd5Hash.toString();
    }
}