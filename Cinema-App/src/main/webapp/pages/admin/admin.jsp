<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center">
<div class="articles_admin">
    <div class="article_admin">
        <div class="item">
            <h1>Редактировать фильмы:</h1>
        </div>
        <div class="description">
            <ul class="header">
                <li style="width: 10%;">Название</li>
                <li style="width: 10%;">Длительность</li>
                <li style="width: 10%;">Жанр</li>
                <li style="width: 10%;">Описание</li>
                <li style="width: 11%;">Рейтинг</li>
                <li style="width: 10%;">Дата начала проката</li>
                <li style="width: 10%;">Дата конца проката</li>
                <li style="width: 10%;">Изменить</li>
                <li style="width: 10%;">Удалить</li>
            </ul>
                <c:forEach var="movie" items="${movieDTOList}">
            <ul class="content">
                    <form action="${pageContext.servletContext.contextPath}/update" method="get" accept-charset='utf-8'>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="title" value="${movie.title}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="duration" value="${movie.duration}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="genre" value="${movie.genre}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="description" value="${movie.description}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="rating" value="${movie.rating}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="start_date" value="${movie.start}"></li>
                    <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="end_date" value="${movie.end}"></li>
                    <li style="width: 10%;"><input type="submit" value="Сохранить" style="width:100px;" ></li>
                    <li style="width: 10%;"><a href="${pageContext.servletContext.contextPath}/delete?movie_id=${movie.id}">Удалить</a></li>
                        <input type="hidden" name="movie_id" value="${movie.id}"/>
                    </form>
            </ul>
                </c:forEach>
            <div align="center"><a href="${pageContext.servletContext.contextPath}/pages/admin/movie_add.jsp">Добавить фильм</a></div>
        </div>
    </div>
    <div class="article_admin">
        <div class="item">
            <h1>Редактировать залы:</h1>
        </div>
        <div class="description">
            <ul class="header">
                <li style="width: 10%;">Название</li>
                <li style="width: 10%;">Изменить</li>
                <li style="width: 10%;">Удалить</li>
            </ul>
            <c:forEach var="hall" items="${hallDTOList}">
                <ul class="content">
                    <form action="${pageContext.servletContext.contextPath}/update" method="get" accept-charset='utf-8'>
                        <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="name" value="${hall.name}"></li>
                        <li style="width: 10%;"><input type="submit" value="Сохранить" style="width:100px;" ></li>
                        <li style="width: 10%;"><a href="${pageContext.servletContext.contextPath}/delete?hall_id=${hall.id}">Удалить</a></li>
                        <input type="hidden" name="hall_id" value="${hall.id}"/>
                    </form>
                </ul>
            </c:forEach>
            <div align="center"><a href="${pageContext.servletContext.contextPath}/pages/admin/hall_add.jsp">Добавить зал</a></div>
        </div>
    </div>
    <div class="article_admin">
        <div class="item">
            <h1>Редактировать сеансы:</h1>
        </div>
        <div class="description">
            <ul class="header">
                <li style="width: 10%;">Фильм</li>
                <li style="width: 10%;">Время</li>
                <li style="width: 10%;">Дата</li>
                <li style="width: 10%;">Цена</li>
                <li style="width: 10%;">Зал</li>
                <li style="width: 10%;">Изменить</li>
                <li style="width: 10%;">Удалить</li>
            </ul>
            <c:forEach var="session" items="${sessionDTOList}">
                <ul class="content">
                    <form action="${pageContext.servletContext.contextPath}/update" method="get" accept-charset='utf-8'>
                        <li style="width: 10%;">${session.movie.title}</li>
                        <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="time" value="${session.sessionTime}"></li>
                        <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="date" value="${session.date}"></li>
                        <li style="width: 10%;"><input type="text" class="text" style="width: 100%;" name="price" value="${session.price}"></li>
                        <li style="width: 10%;">${session.hall.name}</li>
                        <li style="width: 10%;"><input type="submit" value="Сохранить" style="width:100px;" ></li>
                        <li style="width: 10%;"><a href="${pageContext.servletContext.contextPath}/delete?session_id=${session.id}">Удалить</a></li>
                        <input type="hidden" name="session_id" value="${session.id}"/>
                    </form>
                </ul>
            </c:forEach>
            <div align="center"><a href="${pageContext.servletContext.contextPath}/session_add">Добавить сеанс</a></div>
        </div>
    </div>

</div>
</div>


<jsp:include page="../common/Footer.jsp"></jsp:include>