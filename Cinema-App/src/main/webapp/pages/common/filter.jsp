<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="Header.jsp"></jsp:include>
<div align="center">
<div class="articles">

    <div class="article">
        <div class="item">
           По названию
        </div>
        <div class="description">
            <form action='${pageContext.servletContext.contextPath}/moviefilter' id="movie_title_form" method='get' accept-charset="utf-8">
                <div>Введите название:<input type='text'  style='width:100px;height:21px;' name='title'></div><br/>
                <button type='submit'>Отправить</button>
            </form>
        </div>
    </div>

    <div class="article">
        <div class="item">
            По дате начала проката
        </div>
        <div class="description">
        <form action='${pageContext.servletContext.contextPath}/moviefilter' id="movie_start_date_form" method='post' accept-charset="utf-8">
                <div>Введите дату:<input type='text'  style='width:100px;height:21px;' name='start'></div><font size="2pt"></font><br/>
                <button type='submit'>Отправить</button>
        </form>

        </div>
    </div>
    <div class="article">
        <div class="item">
            По дате конца проката
        </div>
        <div class="description">
            <form action='${pageContext.servletContext.contextPath}/moviefilter' id="movie_end_date_form" method='post' accept-charset="utf-8">
                <div>Введите дату:<input type='text'  style='width:100px;height:21px;' name='end'></div><font size="2pt"></font><br/>
                <button type='submit'>Отправить</button>
            </form>

        </div>
    </div>
    </div>

</div>
<jsp:include page="Footer.jsp"></jsp:include>