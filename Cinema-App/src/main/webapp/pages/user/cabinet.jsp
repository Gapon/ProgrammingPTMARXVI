<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center">
    <div>
        <form method="link" action="logout.jsp">

            <input type="submit" value="Logout">
        </form>
    </div>
<h1>Добро пожаловать в кабинет, ${user.name} ${user.surname}!</h1>
    <form action="${pageContext.servletContext.contextPath}/cabinet" method="post" accept-charset="utf-8">
        <h2>Ваши личные данные:</h2>
        <div style="margin-left: 50px;">
            <div style="float:left;text-align:right;margin-top:11px;width:10%;">Фамилия:</div>
            <div style="float:left;text-align:left; margin-top:10px;"><input type="text" class="text" name="surname" value="${user.surname}"></div>
            <div style="float:left;text-align:right; margin-top:11px;width:10%; margin-left: 50px;">Имя:</div>
            <div style="float:left;text-align:left; margin-top:10px;"><input type="text" class="text" name="name" value="${user.name}"></div><br/><br/>
            <div style="float:left;text-align:right; margin-top:11px;">Дата рождения:</div>
            <div style="float:left;text-align:left; margin-top:10px;"><input type="text" class="text" name="birthday" value="${user.birthday}"></div>
            <div style="float:left;text-align:right; margin-top:11px;width:10%; margin-left: 50px;">E-mail:</div>
            <div style="float:left;text-align:left; margin-top:10px;"><input type="text" class="text" name="email" value="${user.email}"></div><br/><br/>
            <div style="float:left;text-align:right; margin-top:11px;width:10%;">Пол:</div>
            <div style="float:left;text-align:left; margin-top:10px;"><input type="text" class="text" name="sex" value="${user.sex}"></div>
            <div style="text-align:center; margin-top:50px;width:50%;"><input type="submit" value="сохранить изменения"></div>
            </div>
        </form>
</div>
<jsp:include page="../common/Footer.jsp"></jsp:include>