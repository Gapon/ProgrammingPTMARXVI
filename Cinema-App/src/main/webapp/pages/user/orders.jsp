<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<c:if test="${ticketByUser ne null}">
<div align="center">
        <h2>Ваши заказы:</h2>

    <ul class="header">
        <li style="width: 12%;">
            Название</li>
        <li style="width: 12%;">
            Время</li>
        <li style="width: 12%;">
            Дата</li>
        <li style="width: 12%;">
            Цена</li>
        <li style="width: 12%;">
            Зал</li>
        <li style="width: 12%;">
            Ряд</li>
        <li style="width: 12%;">
           Место</li>
    </ul>
    <c:forEach var="ticket" items="${ticketByUser}">
        <ul class="content">
            <li style="width: 12%;">${ticket.session.movie.title}</li>
            <li style="width: 12%;">${ticket.session.sessionTime}</li>
            <li style="width: 12%;">${ticket.session.date}</li>
            <li style="width: 12%;">${ticket.session.price}</li>
            <li style="width: 12%;">${ticket.session.hall.name}</li>
            <li style="width: 12%;">${ticket.row}</li>
            <li style="width: 12%;">${ticket.seatNumber}</li>
        </ul>
    </c:forEach>
</div>
</c:if>
<jsp:include page="../common/Footer.jsp"></jsp:include>