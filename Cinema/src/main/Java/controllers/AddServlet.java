package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


@WebServlet(name = "AddServlet", urlPatterns={"/add"})
public class AddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //добавляем фильм
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        String title = request.getParameter("title");
        String duration = request.getParameter("duration");
        String genre = request.getParameter("genre");
        String description = request.getParameter("description");
        String rating = request.getParameter("rating");
        String start_date = request.getParameter("start");
        String end_date = request.getParameter("end");
        for (MovieDTO movieDTO : movieDTOList) {
        if (title != null && !title.equals(movieDTO.getTitle()) && duration != null && genre != null &&
                description != null && rating != null && start_date != null && end_date != null) {
            MovieDTO movie = new MovieDTO(title, description, Integer.valueOf(duration), LocalDate.parse(start_date), LocalDate.parse(end_date), genre, Double.valueOf(rating));
            request.setAttribute("movie", movie);
            MovieServiceImpl.getInstance().upload(movie);
        }
        else {
            request.getSession().setAttribute("message", "Такой фильм уже существует!");
        }
        }
        //добавляем зал
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        String name = request.getParameter("name");
        for (HallDTO hallDTO : hallDTOList) {
        if (name != null && !name.equals(hallDTO.getName())) {
            HallDTO hall = new HallDTO(name);
            request.setAttribute("hall", hall);
            HallServiceImpl.getInstance().upload(hall);
        }
            else {
            request.getSession().setAttribute("message", "Такой зал уже существует!");
        }
        }
        request.getRequestDispatcher("pages/admin/add_true.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }



}
