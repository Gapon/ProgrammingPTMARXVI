package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "DeleteServlet", urlPatterns={"/delete"})
public class DeleteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем из запроса id
        String movie_id = request.getParameter("movie_id");
        String hall_id = request.getParameter("hall_id");
        String session_id = request.getParameter("session_id");
        //удаляем фильм по id
        if (movie_id != null) {
            MovieDTO movie = MovieServiceImpl.getInstance().getById(Integer.valueOf(movie_id));
            MovieServiceImpl.getInstance().delete(Integer.valueOf(movie_id));
            request.setAttribute("movie", movie);
        }
        //удаляем зал по id
        else  if (hall_id != null) {
            HallDTO hall = HallServiceImpl.getInstance().getById(Integer.valueOf(hall_id));
            HallServiceImpl.getInstance().delete(Integer.valueOf(hall_id));
            request.setAttribute("hall", hall);
        }
        //удаляем сеанс по id
        else  if (session_id != null) {
            SessionDTO session = SessionServiceImpl.getInstance().getById(Integer.valueOf(session_id));
            SessionServiceImpl.getInstance().delete(Integer.valueOf(session_id));
            request.setAttribute("session", session);
        }
        request.getRequestDispatcher("pages/admin/delete.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
