package controllers;

import dto.RowDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.RowServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "hallSchemaServlet", urlPatterns={"/hallSchema"})
public class HallSchemaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем id зала
        String hall_id = request.getParameter("hall_id");
        //получаем ряды из базы по залу
        List<RowDTO> rowDTO = RowServiceImpl.getInstance().getRowByHallId(Integer.valueOf(hall_id));
        request.setAttribute("seat_number", rowDTO.get(0).getSeatNumber());
        request.setAttribute("rowDTO", rowDTO);
        String session_id = request.getParameter("session_id");
        String user_id = request.getParameter("user_id");
        request.setAttribute("session_id", session_id);
//        request.setAttribute("user_id", user_id);


        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        if (user != null) {
            //получаем список купленных билетов из базы
            List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getTicketBySession(Integer.valueOf(session_id));
            request.setAttribute("ticketDTOList", ticketDTOList);
            String visibility = null;
            Integer liId = 0;
            List<Integer> idList = new ArrayList<>();
            //определяем id каждого места, чтобы сравнить с id места в схеме зала и сделать место неактивным
        for (TicketDTO ticketDTO : ticketDTOList) {
            for (int i = 0; i < rowDTO.size() ; i++) {
                String row = String.valueOf(i+1);
                for (int j = 1; j <= rowDTO.get(i).getSeatNumber(); j++) {
                    if ((ticketDTO.getRow() == rowDTO.get(i).getRowNumber()) && (ticketDTO.getSeatNumber() == j)) {
                        String seat = String.valueOf(j);
                        String id = row.concat(seat);
                        liId = Integer.valueOf(id);
                        idList.add(liId);
                    }
                    }
                }
            }
            request.setAttribute("idList", idList);
        request.getRequestDispatcher("pages/user/hallSchema.jsp").forward(request, response);
        }
        else {
            request.getSession().setAttribute("message", "Вы не зарегистрированы!");
            response.sendRedirect(request.getContextPath() + "/login.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
