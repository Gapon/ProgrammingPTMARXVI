package controllers;

import dao.impl.UserDaoImpl;
import dto.UserDTO;
import model.User;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;


@WebServlet(name = "LoginServlet", urlPatterns={"/login"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем юзера из базы по логину
        UserDaoImpl userDaoImpl = new UserDaoImpl(User.class);
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        UserDTO user = UserServiceImpl.getInstance().getUserByLogin(login);




        try {
            //проверка на совпадение пароля
            if(user != null && user.getPassword().equals(userDaoImpl.encryptPassword(password))){
                request.getSession().setAttribute("user", user);
                try {
                    response.sendRedirect(request.getContextPath());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }else{
                request.getSession().setAttribute("message", "Неверный логин или пароль!");
                response.sendRedirect("/login.jsp");
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
