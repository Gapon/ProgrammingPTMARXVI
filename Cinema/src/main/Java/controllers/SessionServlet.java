package controllers;


import dto.SessionDTO;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SessionServlet", urlPatterns={"/session"})
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем сеанс из базы по id фильма
        List<SessionDTO> sessionList = SessionServiceImpl.getInstance().getSessionByMovieId(Integer.valueOf(request.getParameter("movie_id")));
        request.setAttribute("sessionList", sessionList);
        request.getRequestDispatcher("pages/common/session.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
