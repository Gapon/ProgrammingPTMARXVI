package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.MovieDaoImpl;
import dto.MovieDTO;
import mapper.BeanMapper;
import model.Movie;
import service.api.Service;

import java.util.List;

public class MovieServiceImpl implements Service<Integer, MovieDTO> {

    private static MovieServiceImpl service;
    private Dao<Integer, Movie> movieDao;
    private MovieDaoImpl moviDaoImpl;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getInstance().getMovieDao();
        beanMapper = BeanMapper.getInstance();
        moviDaoImpl = new MovieDaoImpl(Movie.class);
    }

    public static synchronized MovieServiceImpl getInstance() {
        if (service == null) {
            service = new MovieServiceImpl();
        }
        return service;
    }

    @Override
    public List<MovieDTO> getAll() {
        List<Movie> movies = movieDao.getAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
        return movieDTOs;
    }

    @Override
    public void upload(MovieDTO movieDto) {
        Movie movie = beanMapper.singleMapper(movieDto, Movie.class);
        movieDao.upload(movie);
    }

    @Override
    public MovieDTO getById(Integer id) {
        Movie movie = movieDao.getById(id);
        MovieDTO movieDTO = beanMapper.singleMapper(movie, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void delete(Integer key) {
        movieDao.delete(key);
    }

    @Override
    public void update(MovieDTO movieDto) {
        Movie movie = beanMapper.singleMapper(movieDto, Movie.class);
        movieDao.update(movie);
    }

    public List<MovieDTO> getMovieByStartDate(String start) {
        List<Movie> movie = moviDaoImpl.getMovieByStartDate(start);
        List<MovieDTO> movieDTOList = beanMapper.listMapToList(movie, MovieDTO.class);
        return movieDTOList;
    }

    public List<MovieDTO> getMovieByEndDate(String start) {
        List<Movie> movie = moviDaoImpl.getMovieByEndDate(start);
        List<MovieDTO> movieDTOList = beanMapper.listMapToList(movie, MovieDTO.class);
        return movieDTOList;
    }

    public List<MovieDTO> getMovieByTitle(String title) {
        List<Movie> movie  = moviDaoImpl.getMovieByTitle(title);
        List<MovieDTO> movieDTOList = beanMapper.listMapToList(movie, MovieDTO.class);
        return movieDTOList;
    }

}
