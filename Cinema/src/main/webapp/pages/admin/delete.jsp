<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center">
    <c:if test="${movie ne null}">
    <h2><font color="red"> Фильм ${movie.title} удален из базы!</font></h2>
    </c:if>
    <c:if test="${hall ne null}">
    <h2><font color="red"> Зал ${hall.name} удален из базы!</font></h2>
    </c:if>
    <c:if test="${session ne null}">
    <h2><font color="red"> Сеанс удален из базы!</font></h2>
    </c:if>
    </div>
    <jsp:include page="../common/Footer.jsp"></jsp:include>
