<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center">
    <form action="${pageContext.servletContext.contextPath}/session_add_true" method="get" id="session_add" name="test" accept-charset="utf-8">
        <center>Добавить сеанс:</center>
        <div style="width:100%; float:left; margin-top:5px;">
        <select name="hall_name">
            <c:forEach var="hall" items="${hallList}">
                <option value="${hall.name}">${hall.name}</option>
            </c:forEach>
        </select>
        </div>
        <div style="width:100%; float:left; margin-top:5px;">
        <select name="movie_name">
            <c:forEach var="movie" items="${movieList}">
                <option value="${movie.title}">${movie.title}</option>
            </c:forEach>
        </select>
        </div>
        <div style="width:100%; float:left; margin-top:5px;">
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Время: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="time" style="width:200px;"  placeholder=""/></div>
        </div>
        <div style="width:100%; float:left; margin-top:5px;">
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Дата: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="date" style="width:200px;"  placeholder=""/></div>
        </div>
        <div style="width:100%; float:left; margin-top:5px;">
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Цена: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="price" style="width:200px;" placeholder=""/></div>
        </div>

            <div style="padding-top: 160px;">
                <input type="submit" value="Добавить"/>
            </div>

    </form>
</div>
<jsp:include page="../common/Footer.jsp"></jsp:include>
