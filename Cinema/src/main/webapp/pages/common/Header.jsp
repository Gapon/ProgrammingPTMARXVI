<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <style>
        <%@include file='../css/style.css'%>
        <%@include file='../css/slider.css'%>
     </style>
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
     <script type="text/javascript">
     <%@include file='../js/slider.js'%>
    <%@include file='../js/slide_button.js'%>
    <%@include file='../js/drop_down.js'%>
    <%@include file='../js/drop_down_admin.js'%>
     <%@include file='../js/form_validate.js'%>
    </script>

    <title>Cinema</title>
</head>
<body>
<div>
    <div class="header_container">
       <% HttpSession newsession = request.getSession(false);
        if (newsession != null && request.getSession().getAttribute("user")!=null)
        {
            out.println("<div class='login_menu'><div align = 'center' style='padding-top:20px;'>Добро пожаловать, </br>");%>${user.name} ${user.surname}<%out.println("!</div></div>");
        }
       else {
            out.println("<div class='login_menu'>\n" +
                    "    <div>\n" +
                    "        <form action='");%>${pageContext.servletContext.contextPath}<%out.println("/login' method='post' accept-charset='utf-8'>\n" +
                    "        <div style='float:left;'><font size='3pt'><span>&nbspЛогин&nbsp&nbsp&nbsp </span></font></div>\n" +
                    "            <div><input type='text'  class='text' style='width:70;height:21;' name='login'></div><br/>\n" +
                    "        <div style='float:left;'><font size='3pt'><span>&nbspПароль&nbsp </span></font></div>\n" +
                    "        <div style='float:left;'><input type='password' class='text' style='width:70;height:21;' name='password'></div>&nbsp\n" +
                    "        <div class='submit' ><button type='submit'><img src='");%>${pageContext.servletContext.contextPath}<%out.println("/pages/images/login.gif' alt=''>\n" +
                    "        </button></div>\n" +
                    "        <a href='");%>${pageContext.servletContext.contextPath}<%out.println("/pages/common/register.jsp' STYLE=\"margin-top: -20px;\"><b>Регистрация</b></a>\n" +
                    "        </form>\n" +
                    "    </div>\n" +
                    "</div>");
        }
       %>

        <%--<c:if test="${user ne null}">--%>
            <%--<div class='login_menu'><div align = 'center' style='padding-top:20px;'>Добро пожаловать, </br>${user.name} ${user.surname}!</div></div>--%>
        <%--</c:if>--%>
        <%--<c:if test="${newsession eq null and user eq null}">--%>
            <%--<div class='login_menu'>--%>
            <%--<div>--%>
            <%--<form action='${pageContext.servletContext.contextPath}/login' method='post'>--%>
                <%--<div style='float:left;'><font size='3pt'><span>&nbspЛогин&nbsp&nbsp&nbsp </span></font></div>--%>
                <%--<div><input type='text'  class='text' style='width:70;height:21;' name='login'></div><br/>--%>
                <%--<div style='float:left;'><font size='3pt'><span>&nbspПароль&nbsp </span></font></div>--%>
                <%--<div style='float:left;'><input type='password' class='text' style='width:70;height:21;' name='password'></div>&nbsp--%>
                <%--<div class='submit' ><button type='submit'><img src='${pageContext.servletContext.contextPath}/pages/images/login.gif' alt=''>--%>
                <%--</button></div>--%>
                <%--<a href='${pageContext.servletContext.contextPath}/pages/common/register.jsp' STYLE="margin-top: -20px;"><b>Регистрация</b></a>--%>
                <%--</form>--%>
                <%--</div>--%>
                <%--</div>--%>
        <%--</c:if>--%>

<div class="title" style="text-align: center;"><a href="${pageContext.servletContext.contextPath}"><font face="Arial" size="9pt" ><i><b>Кинотеатр "Киноленд"</b></i></font></a></div>
    <div class="menu_container">
        <ul class="menu">
            <li style="width:19%;">
               <a href="${pageContext.servletContext.contextPath}/pages/common/filter.jsp">Фильтр по фильмам</a>
            </li>
            <li style="width:19%;">
                <a href="${pageContext.servletContext.contextPath}/pages/user/cabinet.jsp">Личный Кабинет</a>
            </li>
            <li style="width:19%;">
                <a href="${pageContext.servletContext.contextPath}/orders">Заказы</a>
            </li>
            <li style="width:19%;">
                <a href="${pageContext.servletContext.contextPath}/pages/user/logout.jsp">Выход</a>
            </li>
            <c:set var="admin" value="admin"></c:set>
            <c:if test="${user.role.name eq admin}">
                <li style="width:19%;">
                <a href="${pageContext.servletContext.contextPath}/admin">Админка</a>
                </li>
            </c:if>
        </ul>
    </div>
</div>

