<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="Header.jsp"></jsp:include>
<div align="center">
    <ul class="header">
        <li style="width: 12%;">
            Название</li>
        <li style="width: 12%;">
            Длительность</li>
        <li style="width: 12%;">
            Жанр</li>
        <li style="width: 12%;">
            Описание</li>
        <li style="width: 12%;">
            Рейтинг</li>
        <li style="width: 12%;">
            Дата начала проката</li>
        <li style="width: 12%;">
            Дата конца проката</li>
    </ul>
    <c:forEach var="mov" items="${movieListByStartDate}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
    <c:forEach var="mov" items="${movieListByEndDate}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
    <c:forEach var="mov" items="${movieByTitle}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
</div>
<jsp:include page="Footer.jsp"></jsp:include>