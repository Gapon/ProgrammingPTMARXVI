<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="Header.jsp"></jsp:include>
<div align="center">
    <!--Слайдер картинок -->
    <%--<div class="arrows">--%>
        <%--<div class="arrownext"></div>--%>
        <%--<div class="arrowprev"></div>--%>
    <%--</div>--%>
    <h2 align="center" style="margin-top: 15px;">Добро пожаловать в наш кинотеатр!</h2>
    <div class="slider">
        <ul>
            <li><img src="pages/images/Alice_Looking_Glass_IMAX_1600x1040.jpg" alt=""></li>
            <li><img src="pages/images/now-you-see-me-the-second-act-afisha2.jpg" alt=""></li>
            <li><img src="pages/images/THE-NICE-GUYS_afisha.jpg" alt=""></li>
            <li><img src="pages/images/tmnt_2-afisha.jpg" alt=""></li>
            <li><img src="pages/images/Wacraft_IMAX_1600x1040.jpg" alt=""></li>
        </ul>
    </div>

    <h1>Вот список доступных к просмотру фильмов:</h1>
    <div class="container_1" style="display: none;">
    <ul class="header" style="padding-left: 0px;">
        <li style="width: 13%;">Название</li>
        <li style="width: 13%;">Длительность</li>
        <li style="width: 13%;">Жанр</li>
        <li style="width: 13%;">Описание</li>
        <li style="width: 13%;">Рейтинг</li>
        <li style="width: 13%;">Дата начала проката</li>
        <li style="width: 13%;">Дата конца проката</li>
    </ul>
    <c:forEach var="movie" items="${movieDTOList}">
    <ul class="content" style="padding-left: 0px;">
        <li style="width: 13%;"><a href="${pageContext.servletContext.contextPath}/movie?id=${movie.id}">${movie.title}</a></li>
        <li style="width: 13%;">${movie.duration}</li>
        <li style="width: 13%;">${movie.genre}</li>
        <li style="width: 13%;">${movie.description}</li>
        <li style="width: 13%;">${movie.rating}</li>
        <li style="width: 13%;">${movie.start}</li>
        <li style="width: 13%;">${movie.end}</li>
    </ul>
    </c:forEach>
        </div>
        <a class="btn-slide" href="#"></a>
</div>
<jsp:include page="Footer.jsp"></jsp:include>