<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="Header.jsp"></jsp:include>
<div align="center">
    <h3>Вот информация по сеансам:</h3>
    <c:forEach var="session" items="${sessionList}">
        <div style="width: 50%;">
        <div style="float: left">
        <div class="show_info_session">
            <i>Время: </i>${session.sessionTime}
        </div>
        <div class="show_info_session">
            <i>Дата: </i>${session.date}
        </div>
        <div class="show_info_session">
            <i>Цена: </i>${session.price}
        </div>
        <div class="show_info_session">
            <i>Зал: </i>${session.hall.name}
        </div>
            <a href="${pageContext.servletContext.contextPath}/hallSchema?hall_id=${session.hall.id}&session_id=${session.id}&user_id=${sessionScope.user.id}">Cхема зала</a>
        </div>
        </div>
    </c:forEach>
    </br></br></br></br></br></br>
    <h3>Внимание! Перед покупкой билета необходимо <a href="${pageContext.servletContext.contextPath}/pages/common/register.jsp"><b>зарегистрироваться</b></a></h3>
</div>
<jsp:include page="Footer.jsp"></jsp:include>