$(document).ready(function() {
 $(".slider").each(function () { // обрабатываем каждый слайдер,  это на случай, если слайдеров картинок несколько
 var obj = $(this);
  $(obj).append("<div class='nav'></div>");								// добавляем блок навигации				
  $(obj).find("li").each(function () {									//применяем функцию для каждого найденного элемента li внутри .slider
   $(obj).find(".nav").append("<span rel='"+$(this).index()+"'></span>"); //добавляем в блок навигации элемент span с значением rel=порядковый номер элемента
   $(this).addClass("slider"+$(this).index());							//добавляем каждому элементу li класс с порядковым номером: slider0, slider1 и т.д
  });
  $(obj).find("span").first().addClass("on"); // делаем активным первый элемент меню
 });

//Автопрокрутка вперед
	var autoscrollnext = function(){
	var obj1=$(".slider .nav span.on");
	var n = obj1.next();	//находим следующий за активным кружочком навигации кружочек (раздел span)
	if(n.length==0) {							//если он последний - переходим на первый
		n=$(".slider .nav span").first();
	}
	n.click();									//реализуем действие клика мышки
	return false;
	}
	
//Функция для прокрутки назад
	var autoscrollprev = function(){
	var obj2=$(".slider .nav span.on");
	var n1 =obj2.prev();
	if(n1.length==0) {							
		n1=$(".slider .nav span").last();
	}
	n1.click();									
	return false;
	}

	var inter = setInterval(autoscrollnext,5000);

$(document).on("click", ".slider .nav span", function() { // запускаем функцию при событии click для элементов span с классом nav внутри элемента с классом slider
 var sl = $(this).closest(".slider"); // находим, в каком блоке был клик (ближайший к нажатому блок с классом slider) это на случай, если слайдеров картинок несколько
 $(sl).find("span").removeClass("on"); // убираем активный элемент блока управления со всех остальных
 $(this).addClass("on"); // делаем активным текущий элемент блока управления
 var obj = $(this).attr("rel"); // узнаем его номер (0,1,2...)
 sliderJS(obj, sl); // слайдим
 return false;
});


function sliderJS (obj, sl) { // slider function
 var ul = $(sl).find("ul"); // находим блок
 var bl = $(sl).find("li.slider"+obj); // находим любой из элементов блока по его классу slider0 (slider + 0) или др.
 var step = $(bl).width(); // ширина объекта
 $(ul).animate({marginLeft: "-"+step*obj}, 500); // 500 это скорость перемотки
 
};
	
$(document).on("click", ".arrownext", function() {				//прокрутка при нажатии на кнопку вперед
clearInterval(inter);
autoscrollnext();
});	

$(document).on("click", ".arrowprev", function() {				//прокрутка при нажатии на кнопку назад
clearInterval(inter);
autoscrollprev();
});

});

