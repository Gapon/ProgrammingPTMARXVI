package HomeWork2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtils {
    public static void main(String[] args) {
        StringUtils stringUtils = new StringUtils();
        stringUtils.stringReverseSB("Вася любит Машу");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.isPalindromeString("А роза упала на лапу Азора");
        stringUtils.isPalindromeString("А роза не упала на лапу Азора");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.stringLengthTest("KolyaPetyaVasya");
        stringUtils.stringLengthTest("KolyaPetya");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.replaceFirstWordWithLast("Вася любит свою маму");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.replaceFirstWordWithLastInEachSentense("Вася любит свою маму.А мама Васю");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.findABC("aaababbbcbccb");
        stringUtils.findABC("aaababbdregedvbcbccb");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.dateCheker("12.12.2014");
        stringUtils.dateCheker("ds12.12.2014sdf");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.emailChecker("vasya@pupkin.ru");
        stringUtils.emailChecker("katya.ivanova@mail.ru");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stringUtils.phoneNumberSearcher("Kolya number: +7(345)312-13-14, Vasya number: +1(356)213-35-67");

    }

    //переворачивание строки наоборот
    public String stringReverseSB(String s) {
        StringBuilder sb = new StringBuilder();
        System.out.println("Cтрока наоборот: ");
        for (int i = s.length(); i > 0; i--) {
            System.out.print(s.charAt(i - 1));
            sb.append(s.charAt(i-1));
        }
        System.out.println();
        return  sb.toString();
    }

    //проверка на палиндром
    public boolean isPalindromeString(String str) {
        String str2 = str.replaceAll(" ", "").toLowerCase();
        if (str == null)
            return false;
        for (int i = 0; i < str2.length() / 2; i++) {
            if (str2.charAt(i) != str2.charAt(str2.length() - i - 1)) {
                System.out.println("Строка: \"" + str + "\" не является палиндромом");
                return false;
            }
        }
        System.out.println("Строка: \"" + str + "\" является палиндромом");
        return true;
    }

    //проверка на длину строки и выполнение действий
    public String stringLengthTest(String s) {
        String s1= "";
        if (s.length() > 10) {
            System.out.println(s1 = s.substring(0, 6));
            return s1 = s.substring(0, 6);
        }
        else {
            while (s.length() < 12)
                s = s.concat("o");
        }
        System.out.println(s);
        return s;
    }

    //поменять местами 1-е и последнее слово
    public String replaceFirstWordWithLast (String str) {
        int firstSpace = str.indexOf(" ");
        int lastSpace = str.lastIndexOf(" ");
        String firstWord = str.substring(0, firstSpace);
        String lastWord = str.substring(lastSpace+1, str.length());
        String rest = str.substring(firstSpace, lastSpace + 1);
        str = lastWord + rest + firstWord;
        System.out.println(str);
        return str;
    }

    //поменять местами 1-е и последнее слово в каждом предожении в строке, разделенном точкой.
    public String[] replaceFirstWordWithLastInEachSentense(String str) {
        String[] str2 = str.split("[.]+");
        String[] str3 = new String[str2.length];
        for (int i = 0; i < str2.length; i++) {
            int firstSpace = str2[i].indexOf(" ");
            int lastSpace = str2[i].lastIndexOf(" ");
            String firstWord = str2[i].substring(0, firstSpace);
            String lastWord = str2[i].substring(lastSpace+1, str2[i].length());
            String rest = str2[i].substring(firstSpace, lastSpace + 1);
            str3[i] = lastWord.concat(rest).concat(firstWord);
        }
        for (int i = 0; i < str3.length; i++) {
            if (i != str3.length - 1) {
                str3[i] = str3[i].concat(".");
            }
            System.out.print(str3[i]);

    }
        System.out.println();
        return str3;
    }

    //проверка на содержание a b c
    public boolean findABC (String str) {
        Pattern pattern = Pattern.compile("[abc]+");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            System.out.println("Строка: \"" + str + "\" содержит только a b c");
            return true;
        }
        else System.out.println("Строка: \"" + str + "\" содержит не только a b c");
        return false;
    }

    //проверка  является ли строчка датой формата MM.DD.YYYY
    public boolean dateCheker (String str) {
        Pattern pattern = Pattern.compile("\\b\\d{0,2}\\.\\d{0,2}\\.\\d{0,4}\\b");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            System.out.println("Строка: \"" + str + "\" является датой формата MM.DD.YYYY");
            return  true;
        }
        else System.out.println("Строка: \"" + str + "\" не является датой формата MM.DD.YYYY");
        return false;
    }

    //проверка на почтовый адрес
    public boolean emailChecker (String str) {
        Pattern pattern = Pattern.compile("\\w+\\.?\\w+@\\w+\\.\\w{0,3}");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            System.out.println("Строка: \"" + str + "\" является e-mail");
            return true;
        }
        else System.out.println("Строка: \"" + str + "\" не является e-mail");
        return false;
    }

    //нахождение телефонов в тексте и запись в массив
    public List phoneNumberSearcher (String str) {
       List<String> phoneNumbers = new ArrayList<>();
        Pattern pattern = Pattern.compile("(\\+\\s?\\d{0,3}\\s?)\\(?(\\d{0,3})\\)?\\s?(\\d{0,3}\\s?)\\-?(\\d{0,2}\\s?)\\-?(\\d{0,2}\\b)");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            String result = matcher.group(1).concat(matcher.group(2)).concat(matcher.group(3)).concat(matcher.group(4)).concat(matcher.group(5));
            phoneNumbers.add(result);
        }
        for (String phoneNumber : phoneNumbers) {
            System.out.println(phoneNumber);
        }
        return phoneNumbers;
    }

}


