package HomeWork3;

import java.util.Arrays;

public class GenericStorage<T> {
    private T [] arr; //массив
    private int index = 0; //индекс текущего элемента массива
    //конструктор без параметров задает размер 10
    public GenericStorage() {
        this.setArr((T[]) new Object[10]);
    }
    //конструктор с размером
    public GenericStorage(int size) {
        this.setArr((T[]) new Object[size]);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public T [] getArr() {
        return arr;
    }

    public void setArr(T [] arr) {
        this.arr = arr;
    }

    //добавление в конец массива
    public boolean add(T obj) {
        if (getIndex()<=getArr().length) {
            getArr()[getIndex()] = obj;
            setIndex(getIndex() + 1);//увеличиваем индекс на 1
        }
        return true;
    }

    //возврат элемента по индексу
    public T get(int index) {
        if (arr.length != 0 && (index >= 0 && index <= getIndex())) {
            System.out.println((index + 1) + "-й элемент массива: " + getArr()[index]);
            return getArr()[index];
        } else System.out.println("Массив 0-й длины. Метод нельзя вызвать!");
        return null;
    }

    //возврат всего массива
    public String getAll() {
        final StringBuilder sb = new StringBuilder("Массив: ");
        sb.append(Arrays.toString(getArr()));
        return sb.toString();
    }

    //замена элемента на новый по заданному индексу
    public boolean update (int index, T obj){
        System.out.println("Меняем " + (index+1) + " элемент на \"" + obj + "\"");
        if (arr.length != 0  && (index >= 0 && index <= getIndex())) {
            if (arr[index] != null) {
                getArr()[index] = obj;
                return true;
            }
        }
        else System.out.println("Проверьте индекс! Элемент с таким индексом не найден");
            return false;
    }

    //удаление объекта по индексу
    public boolean delete(int index) {
        if (arr.length != 0  && (index >= 0 && index <= getIndex())) {
            //getArr().length-1-id - кол-во элементов, которые будут передвинуты влево
            System.arraycopy(getArr(), index + 1, getArr(), index, getArr().length - 1 - index);//перемещаем массив на 1 элемент влево, стирая один элемент
            getArr()[getArr().length-1] = null;
            setIndex(getIndex() - 1);//уменьшаем индекс на 1
            System.out.println("Удаляем " + (index + 1) + "-й элемент");
            return true;
        } else System.out.println("Проверьте индекс! Элемент с таким индексом не найден");
        return false;
    }

    //удаление всех вхождений заданного объекта
    public boolean delete(T obj) {
        if (arr.length != 0) {
            for (int i = 0; i < getIndex(); i++) {
                if (i == getIndex()-1){
                    getArr()[i] = null;
                    return true;
                }
                while (getArr()[i].equals(obj)) {
                    //getArr().length-1-id - кол-во элементов, которые будут передвинуты влево System.arraycopy();
                    System.arraycopy(getArr(), i+1, getArr(), i, getArr().length - 1-i);//перемещаем массив на 1 элемент влево, стирая один элемент
                    setIndex(getIndex() - 1);//уменьшаем индекс на 1
                    System.out.println("Удаляем элемент: \"" + obj + "\"");
                }
            }
            return true;
        } else System.out.println("Массив 0-й длины. Метод нельзя вызвать!");
        return false;
    }

    //возврат длины заполненного массива
    public int usedArrayLength(){
        int usedLenght = 0;
        for (T t : arr) {
            if (t != null) usedLenght++;
        }
        System.out.print("Размер заполненного массива: " + usedLenght + "\n");
        return usedLenght;
    }
}
