package HomeWork4_1;

import java.util.*;


public class App{
    public static void main(String[] args) {
        Car porshe = new Car("Porshe", 50);
        Car vw = new Car("VW", 30);
        Car lexus = new Car("Lexus", 70);
        Car[] cars = {porshe,vw,lexus};

        Computer lenovo = new Computer("Lenovo", 4000.0);
        Computer asus = new Computer("Asus", 4500.0);
        Computer apple = new Computer("Apple", 8000.0);
        Computer[] computers = {lenovo,asus,apple};

        String[] strings = {"Vasya","Petya","Kolya"};
        Integer[] integer = {1,2,3,4,5};

        App.compare(strings);
        App.compare(computers);
        App.compare(integer);
//        BookApp.compare(cars);    это не работает, так как клаcc Car не реализует Comparable!
    }


    public static <T extends Comparable>  void compare(T[] arr) {
        try {
                Arrays.sort(arr, (o1, o2) -> o1.compareTo(o2));
            System.out.println("Максимальный элемент: " + arr[arr.length-1]);
        } catch (Exception e) {
            System.out.println(arr.getClass().getSimpleName() + ": Этот класс не реализовывает Comparable!");
        }

    }
}





