package HomeWork4_1;


public class Computer implements Comparable<Computer>{

    private String model;
    private Double price;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Computer(String model, Double price) {
        this.model = model;
        this.price = price;
    }


    @Override
    public int compareTo(Computer o) {
        return getPrice().compareTo(o.getPrice());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Computer: ");
        sb.append("model='").append(model).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
