package HomeWork4_2.part1;


public class Demo {

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(423);
        deque.addFirst(453);
        deque.addLast(8.88);
        deque.addLast(345);
        System.out.println(deque);
        System.out.println("Список содержит 433: " + deque.contains(433));
        System.out.println("Первый элемент списка:" + deque.getFirst());
        System.out.println("Последний элемент списка:" + deque.getLast());
//        deque.removeFirst();
//        deque.removeLast();
//        deque.clear();
        System.out.println("Список содержит 433: " + deque.contains(433));

        MyDeque<Number> deque1 = new MyDequeImpl<>();
        deque1.addFirst(453);
        deque1.addFirst(423);
        deque1.addLast(8.88);
        deque1.addLast(345);
        System.out.println("Список содержит все элементы списка deque: " + deque.containsAll(deque1));
    }

}