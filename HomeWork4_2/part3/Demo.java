package HomeWork4_2.part3;


public class Demo {

    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<Number>();
        deque.addFirst(42343);
        deque.addFirst(453);
        deque.addLast(8.88);
        deque.addLast(345);
        deque.addLast(234234);
        System.out.println(deque);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        ListIterator<Number> listIt = deque.listIterator();

        while (listIt.hasNext()) {
            System.out.println(listIt.next());

        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");

        while (listIt.hasPrevious()) {
            System.out.println(listIt.previous());
          listIt.removePrev();
            if (deque.size() <=2)
                break;
        }
        System.out.println(deque);
    }

}