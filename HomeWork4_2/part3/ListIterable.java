package HomeWork4_2.part3;


public interface ListIterable<E> {

    ListIterator<E> listIterator();

}
