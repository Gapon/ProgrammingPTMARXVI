package HomeWork4_2.part3;

import java.util.Iterator;


public interface ListIterator<E> extends Iterator<E> {

// проверяет, есть ли предыдущий элемент для выборки методом previous

    boolean hasPrevious();

// возвращает предыдущий элемент

    E previous();

// заменяет элемент, который на предыдущем шаге был возвращен next/previous на данный элемент

    void set(E e);

// удаляет элемент, который на предыдущем шаге был возвращен next/previous

    void removePrev();

}
