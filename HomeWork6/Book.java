package HomeWork6;

import java.io.Serializable;

class Book implements Serializable{
    private static final long serialVersionUID = 7681284865565740888L;
    private String title;
    private String author;
    private int year;

    public Book() {
    }

    public Book(String author, String title, int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Book: ");
        sb.append("author - ").append(author);
        sb.append(", title - '").append(title).append('\'');
        sb.append(", year = ").append(year);
        return sb.toString();
    }
}
