package HomeWork6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class BookApp {
    public static void main(String[] args) {
        serialize("BooksData");
    }

    public static List<Book> readFromFile(String path) {
        StringBuilder sb = new StringBuilder();
        List<Book> bookList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));){
            String s;
            while ((s = br.readLine()) != null){
                String[] buffer = s.split(";");
                int year = Integer.valueOf(buffer[2]);
                bookList.add(new Book(buffer[0], buffer[1],year));
            }
            for (Book book : bookList) {
                System.out.println(book);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookList;
    }
    public static void serialize(String path) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("BookSerialize.data"));
            oos.writeObject(readFromFile(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
