package HomeWork6;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;


public class FileRedactor {
    public static void main(String[] args) {
        String path = null;
        String fileName = null;
        String newFileName = null;
        System.out.println("Ввести корневую директорию");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new UnclouseSystemIn(System.in)))) {
        try {
            path = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m:
        for (; ; ) {
            System.out.println();
            System.out.println("Меню:");
            System.out.println("1. Создать файл");
            System.out.println("2. Переименовать файл");
            System.out.println("3. Удалить файл");
            System.out.println("4. Показать все файлы директории");
            System.out.println("0. Выход");
            //начало меню

            int menu = 0;
            try {
                menu = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }

            switch (menu) {
                case 1:
                    System.out.println("Введите имя файла:");
                    try {
                        fileName = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    createFile(fileName, path);
                    break;
                case 2:
                    System.out.println("Введите имя файла для переименования:");
                    try {
                        fileName = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Введите нове имя файла:");
                    try {
                        newFileName = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    renameFile(fileName, newFileName, path);
                    break;
                case 3:
                    System.out.println("Введите имя файла для удаления:");
                    try {
                        fileName = reader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    deleteFile(fileName, path);
                    break;
                case 4:
                    showDirectory(path);
                    break;
                case 0:
                    break m;
            }

        }
    } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void showDirectory(String fileName){
            File dir = new File(fileName);
            if (dir.isDirectory()) {
                // получаем все вложенные объекты в каталоге
                for (File item : dir.listFiles()) {
                    if (item.isDirectory()) {
                        System.out.println(item.getName() + " - \t\tкаталог");
                    } else {
                        System.out.println(item.getName() + " - \t\tфайл");
                    }
                }
            }
        }

    public static void createFile (String fileName, String directory) {
        File newFile = new File(directory.concat("\\").concat(fileName));
        try {
            boolean created = newFile.createNewFile();
            if (created)
                System.out.println("Файл \"" + fileName + "\" создан");
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void renameFile (String fileName, String newFileName, String directory){
        File sourse = new File(directory.concat("\\").concat(fileName));
        File destination = new File(directory.concat("\\").concat(newFileName));
        if (sourse.exists()){
            sourse.renameTo(destination);
            System.out.println("Файл \"" + fileName + "\" переименован в \"" + newFileName + "\"");
        }
    }

    public static void deleteFile (String fileName, String directory){
        File file = new File(directory.concat("\\").concat(fileName));
        boolean deleted = file.delete();
        if(deleted)
            System.out.println("Файл \"" + fileName + "\" удален");
    }
}
