package HomeWork7;


public class CounterApp {
    public static void main(String[] args) {
        notSyncronizedCounter();
        syncronizedCounter();
    }
    //не синхронизированный метод
    public static void notSyncronizedCounter() {
        Counter threadCounter = new Counter();
        System.out.println("Не синхронизированный блок:");
        new Thread() {
            @Override
            public void run() {
                try {
                    int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                    System.out.println(result);
                    threadCounter.setCounter1();
                    sleep(10);
                    threadCounter.setCounter2();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                try {
                    int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                    System.out.println(result);
                    threadCounter.setCounter1();
                    sleep(10);
                    threadCounter.setCounter1();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                try {
                    int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                    System.out.println(result);
                    threadCounter.setCounter1();
                    sleep(10);
                    threadCounter.setCounter1();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    //синхронизированный метод
    public static void syncronizedCounter() {
        try {
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Counter threadCounter = new Counter();
        System.out.println("Синхронизированный блок:");
        new Thread() {
            @Override
            public void run() {
                synchronized (threadCounter) {
                    try {
                        int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                        System.out.println(result);
                        threadCounter.setCounter1();
                        sleep(10);
                        threadCounter.setCounter2();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                synchronized (threadCounter) {
                    try {
                        int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                        System.out.println(result);
                        threadCounter.setCounter1();
                        sleep(10);
                        threadCounter.setCounter2();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                synchronized (threadCounter) {
                    try {
                        int result = threadCounter.getCounter1() - threadCounter.getCounter2();
                        System.out.println(result);
                        threadCounter.setCounter1();
                        sleep(10);
                        threadCounter.setCounter2();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
