package Practice1;


public class ArraySum {
   
    private int [] numConstructor;


    public int[] getNumConstructor() {
        return numConstructor;
    }

    public void setNumConstructor(int[] numConstructor) {
        this.numConstructor = numConstructor;
    }

 
    public ArraySum(int[] numConstructor) {
        this.setNumConstructor(numConstructor);
    }


    //Метод суммирования массива,
    //заданного в конструкторе
    public int sum() {
        int sum = 0;
        for (int i : numConstructor) {
            sum += i;
        }
        return sum;
    }

  
    //Метод суммирования массива
    public static int sum(int[] num) {
        int sum = 0;
        for (int i : num) {
            sum += i;
        }
        return sum;
    }


}
