package Practice1.Exceptions;


public class ExamNameException extends Throwable {
    public ExamNameException(String message) {
        super(message);
    }
}
