package Practice1.Exceptions;


public class ExamNotFoundException extends Throwable {
    public ExamNotFoundException(String message) {
        super(message);
    }
}
