package Practice1.Exceptions;


public class MarkException extends Throwable {
    public MarkException(String message) {
        super(message);
    }
}
