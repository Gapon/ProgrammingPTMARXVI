package Practice1.Exceptions;


public class SemestrException extends Throwable {
    public SemestrException(String message) {
        super(message);
    }
}
