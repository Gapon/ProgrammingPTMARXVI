package Practice2;


public class Computer {
    private String model;
    private Double price;
    private Double screenDiagonal;
    private String core;
    private int memoryCapacity;
    private int hdd;
    private String resolution;

    public String getCore() {
        return core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public int getHdd() {
        return hdd;
    }

    public void setHdd(int hdd) {
        this.hdd = hdd;
    }

    public int getMemoryCapacity() {
        return memoryCapacity;
    }

    public void setMemoryCapacity(int memoryCapacity) {
        this.memoryCapacity = memoryCapacity;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Double getScreenDiagonal() {
        return screenDiagonal;
    }

    public void setScreenDiagonal(Double screenDiagonal) {
        this.screenDiagonal = screenDiagonal;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Computer: ");
        sb.append("Model = '").append(model).append('\'');
        sb.append(", Processor = ").append(core).append(" ГГц");
        sb.append(", ScreenDiagonal = ").append(screenDiagonal).append("\"");
        sb.append(", MemoryCapacity = ").append(memoryCapacity).append(" Гб");
        sb.append(", HDD = ").append(hdd).append(" Гб(Тб)");
        sb.append(", Resolution = '").append(resolution).append('\'');
        sb.append(", Price = ").append(price).append(" грн.");
        sb.append(';');
        return sb.toString();
    }
}
