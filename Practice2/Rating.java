package Practice2;


public class Rating {
    private String womanName;
    private String manName;
    private int rating;

    public String getManName() {
        return manName;
    }

    public void setManName(String manName) {
        this.manName = manName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getWomanName() {
        return womanName;
    }

    public void setWomanName(String womanName) {
        this.womanName = womanName;
    }

    public Rating(String manName, int rating, String womanName) {
        setManName(manName);
        setRating(rating);
        setWomanName(womanName);
    }

    public Rating() {
    }

    @Override
    public String toString() {
        return "rating = " + rating + ", manName = " + manName + ", womanName = " + womanName + ';';
    }
}
