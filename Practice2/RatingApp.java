package Practice2;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RatingApp {
    private static  List<Rating> ratings = new ArrayList();

    public static List<Rating> getRatings() {
        return ratings;
    }

    public static void addRatings(Rating rating) {
        ratings.add(rating);
    }

    private String INPUTFILEPATH = "baby2008.html";

    public static void main(String[] args) throws FileNotFoundException {
        RatingApp parser = new RatingApp();
        parser.readFromFile();
        parser.parse();
        System.out.println(RatingApp.toStringNew(ratings));
    }
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public  String readFromFile () throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(
                    INPUTFILEPATH),"windows-1251"));
            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setText(sb.toString());
        return sb.toString();
    }


    public List<Rating> parse(){

        Pattern pattern = Pattern.compile("<td>(?<rating>\\d+)<\\/td><td>(?<man>\\w+)<\\/td><td>(?<woman>\\w+)<\\/td>");
        Matcher matcher = pattern.matcher(getText());
        while (matcher.find()) {
            Rating rating = new Rating();
            rating.setRating(Integer.parseInt(matcher.group("rating")));
            rating.setManName(matcher.group("man"));
            rating.setWomanName(matcher.group("woman"));
            addRatings(rating);
        }
        return ratings;
    }

    public static String toStringNew(List<Rating> ratings){
        StringBuilder sb = new StringBuilder();
        for (Rating rating : ratings) {
            sb.append(rating);
            sb.append('\n');
        }
          return sb.toString();
    }
}
