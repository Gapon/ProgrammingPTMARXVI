package Practice3;

import java.util.List;


public class Book {
    private long id;
    private String author;
    private String title;
    private int count;
    private List<User> users;
    private List<Report> reports;

    public Book(String author, int count, long id, List<Report> report, String title, List<User> users) {
        setAuthor(author);
        setCount(count);
        setId(id);
        setReports(report);
        setTitle(title);
        setUsers(users);
    }

    public Book() {}

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    public void addReport ( Report report ) { reports.add(report);}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public  void addUser (User user) { users.add(user);}

}
