package Practice3;

import java.util.Date;


public class Report {
    private long id;
    private Book book;
    private User user;
    private Date rent;
    private Date returns;

    public Report(Book book, long id, Date rent, Date returns, User user) {
      setBook(book);
      setId(id);
      setRent(rent);
      setReturns(returns);
      setUser(user);
    }

    public Report() {}

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getRent() {
        return rent;
    }

    public void setRent(Date rent) {
        this.rent = rent;
    }

    public Date getReturns() {
        return returns;
    }

    public void setReturns(Date returns) {
        this.returns = returns;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
