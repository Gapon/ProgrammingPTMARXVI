package Practice4;
import java.io.*;
import java.util.*;


public class Reader {
    List<String> list = new LinkedList<>();

    public static void main(String[] args) throws FileNotFoundException {
       Reader reader = new Reader();
//       reader.printer(reader.uniqwordsCount("romeo.txt"));
       System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      reader.printer(reader.uniqwordsCount("romeo.txt", Sort.VALUEDOWN));

    }
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    //чтение из файла
    public String readFromFile (String path) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setText(sb.toString());
        return sb.toString();
    }

    //метод для подсчета уникальных слов
    public Map <String, Counter> uniqwordsCount(String path) throws FileNotFoundException {
        String[] splitString = readFromFile(path).split("[ \\n,.?!\\--:;\\[\\]]+");
        Map<String, Counter> words = new HashMap<>();
        for (String s : splitString) {
            if (!words.containsKey(s)){
                words.put(s, new Counter());
            } else {
                words.get(s).increaseCounter();
            }
        }
        return words;
    }
    //метод для сортировки
    public Map <String, Counter> uniqwordsCount(String path, Sort sort) throws FileNotFoundException {
        Map<String, Counter> words = uniqwordsCount(path);
        Comparator<String> comp = null;
        switch (sort) {
            //сортировка по количеству вхождений от наименьшего к наибольшему
            case VALUEUP:
                comp = (key1, key2) -> (words.get(key1).getCounter()- words.get(key2).getCounter()==0) ? 1 : words.get(key1).getCounter()- words.get(key2).getCounter();
                break;
            //сортировка по количеству вхождений от наибольшего к наименьшему
            case VALUEDOWN:
                comp = (key1, key2) -> (words.get(key2).getCounter()- words.get(key1).getCounter()==0) ? 1 : words.get(key2).getCounter()- words.get(key1).getCounter();
                break;
            //сортировка слов по алфавиту
            case KEYUP:
                comp = (value1, value2) -> value1.compareTo(value2); break;
            //сортировка слов по алфавиту в обратном порядке
            case KEYDOWN:
                comp = (value1, value2) -> value2.compareTo(value1); break;
        }
        TreeMap<String, Counter> sortedMap = new TreeMap<>(comp);
        sortedMap.putAll(words);
    return sortedMap;
    }
    //метод для печати
    public void printer(Map <String, Counter> map){
        for (Map.Entry entry : map.entrySet()) {
            System.out.println("Слово " + entry.getKey() + " встречается " + entry.getValue() + " раз(а)");
        }
    }

    private class Counter {
        private int counter = 1;

        public int increaseCounter() {
            counter++;
            return counter;
        }

        public int getCounter() {
            return counter;
        }

        public void setCounter(int counter) {
            this.counter = counter;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(counter);
            return sb.toString();
        }
    }

    private enum Sort{
        VALUEUP, VALUEDOWN, KEYUP, KEYDOWN;
        }
}

