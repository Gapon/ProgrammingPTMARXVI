package Practice5;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID = 7561227128030391595L;
    private String name;
    private String surName;
    private int age;
    private int course;


    public Student(int age, int course, String name, String surName) {
        this.age = age;
        this.course = course;
        this.name = name;
        this.surName = surName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student:");
        sb.append("age = ").append(age);
        sb.append(", name - ").append(name);
        sb.append(", surName - ").append(surName);
        sb.append(", course = ").append(course);
        return sb.toString();
    }

}
