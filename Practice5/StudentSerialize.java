package Practice5;

import java.io.*;

public class StudentSerialize {
    public static void main(String[] args) {
        Group newGroup = new Group();
        Student vasya = new Student(20,3,"Vasya","Petrov");
        Student petya = new Student(22,5,"Petya","Ivanov");
        Student kolya = new Student(21,4,"Kolya","Sidorov");
        newGroup.addStudents(vasya);
        newGroup.addStudents(petya);
        newGroup.addStudents(kolya);

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Group.data"));
            oos.writeObject(newGroup);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Group.data"));
            Group group = (Group) ois.readObject();
            System.out.println(group);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}
