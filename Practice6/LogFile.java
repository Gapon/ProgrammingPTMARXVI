package Practice6;

import java.io.*;


public class LogFile extends Thread {
    public static String SEARCH_PATH = "C:\\Users\\Андрей\\IdeaProjects\\Programming\\src\\main\\resourses\\Practice6\\CopyFrom";
    public static String LOG_PATH = "log.txt";
    //метод для сканирования заданной папки и записи соответствующих отбору данных в лог-файл
    public static void searchFiles(String COPY_FROM_PATH) {
        File[] sourseList = new File(COPY_FROM_PATH).listFiles();
        for (File file : sourseList) {
            if (file.isFile()) {
                    if (file.getName().contains("txt")) {
                        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LOG_PATH, true)))) {
                            bw.write(file.getAbsolutePath() + "\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            }
            if (file.isDirectory()) {
                new Thread(() -> searchFiles(file.getAbsolutePath())).start();
            }
        }
    }

    @Override
    public void run() {
        searchFiles(SEARCH_PATH);
    }

    public static void main(String[] args) {
        Thread logFile = new LogFile();
        logFile.start();
    }
}
