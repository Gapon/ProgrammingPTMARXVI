package Practice6;

import java.util.LinkedHashMap;
import java.util.Map;


public class MySсhedule {
    public static void main(String[] args) {
        Map<String,Integer> map = new LinkedHashMap<>();
        map.put("First message", 1000);
        map.put("Second message", 500);
        map.put("Third message", 3000);
        messagesMap(map);
    }

    public static String messagesMap(Map<String,Integer> messages){
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Integer> entry : messages.entrySet()) {
            try {
                Thread.currentThread().sleep(entry.getValue());
                sb.append(entry.getValue()).append(" ");
                System.out.println(entry.getKey());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
